package inventory.repository;


import inventory.model.InhousePart;
import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.model.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.StringTokenizer;

public class InventoryRepository {

    private static String filename = "data/items.txt";
// Declare fields
    private ObservableList<Product> allProducts;
    private ObservableList<Part> allParts;
    private int autoPartId;
    private int autoProductId;

    private static Logger logger = LoggerFactory.getLogger(InventoryRepository.class);

    public InventoryRepository() {
        this.allProducts = FXCollections.observableArrayList();
        this.allParts= FXCollections.observableArrayList();
        this.autoProductId=0;
        this.autoPartId=0;
        readParts();
        readProducts();
    }

    public void readParts() {
        ClassLoader classLoader = InventoryRepository.class.getClassLoader();
        try {
            InputStream in = classLoader.getResourceAsStream(filename);
            File file = new File(String.valueOf(in));
//            File file = new File(classLoader.getResource(filename).getFile()); Modified replaced by these 2 lines above
            ObservableList<Part> listP = FXCollections.observableArrayList();

            auxReadParts(listP,file);
        }
        catch (NullPointerException ex){
            logger.info("File not found");
        }
    }

    public void auxReadParts(ObservableList<Part> listP,File file){

//        try (BufferedReader br = new BufferedReader(new FileReader(file))){ Modified
        try (BufferedReader br = new BufferedReader(new FileReader("src/main/resources/"+filename))){
           String line = null;
            while ((line = br.readLine()) != null) {
                Part part = getPartFromString(line);
                if (part != null)
                    listP.add(part);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        allParts = listP;
    }



    private Part getPartFromString(String line) {
        Part item = null;
        if (line == null || line.equals("")) return null;
        StringTokenizer st = new StringTokenizer(line, ",");
        String type = st.nextToken();
        if (type.equals("I")) {
            int id = Integer.parseInt(st.nextToken());
            autoPartId=id;
            String name = st.nextToken();
            double price = Double.parseDouble(st.nextToken());
            int inStock = Integer.parseInt(st.nextToken());
            int minStock = Integer.parseInt(st.nextToken());
            int maxStock = Integer.parseInt(st.nextToken());
            int idMachine = Integer.parseInt(st.nextToken());
            item = new InhousePart(id, name, price, inStock, minStock, maxStock, idMachine);
        }
        if (type.equals("O")) {
            int id = Integer.parseInt(st.nextToken());
            autoPartId=id;
            String name = st.nextToken();
            double price = Double.parseDouble(st.nextToken());
            int inStock = Integer.parseInt(st.nextToken());
            int minStock = Integer.parseInt(st.nextToken());
            int maxStock = Integer.parseInt(st.nextToken());
            String company = st.nextToken();
            item = new OutsourcedPart(id, name, price, inStock, minStock, maxStock, company);
        }
        return item;
    }

    public void readProducts() {
        ClassLoader classLoader = InventoryRepository.class.getClassLoader();
        try {
            InputStream in = classLoader.getResourceAsStream(filename);
            File file = new File(String.valueOf(in));
//            File file = new File(classLoader.getResource(filename).getFile()); Modified replaced by these 2 lines above
            ObservableList<Product> listP = FXCollections.observableArrayList();

            auxReadProducts(listP,file);
        }
        catch(NullPointerException ex){
            logger.info("File not found");

        }
    }

    public void auxReadProducts(ObservableList<Product> listP,File file){
//        try (BufferedReader br = new BufferedReader(new FileReader(file))){ Modified
        try (BufferedReader br = new BufferedReader(new FileReader("src/main/resources/"+filename))){
           String line = null;
            while ((line = br.readLine()) != null) {
                Product product = getProductFromString(line);
                if (product != null)
                    listP.add(product);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        allProducts = listP;
    }

    private Product getProductFromString(String line) {
        Product product = null;
        if (line == null || line.equals("")) return null;
        StringTokenizer st = new StringTokenizer(line, ",");
        String type = st.nextToken();
        if (type.equals("P")) {
            int id = Integer.parseInt(st.nextToken());
            autoProductId=id;
            String name = st.nextToken();
            double price = Double.parseDouble(st.nextToken());
            int inStock = Integer.parseInt(st.nextToken());
            int minStock = Integer.parseInt(st.nextToken());
            int maxStock = Integer.parseInt(st.nextToken());
            String partIDs = st.nextToken();

            StringTokenizer ids = new StringTokenizer(partIDs, ":");
            ObservableList<Part> list = FXCollections.observableArrayList();
            while (ids.hasMoreTokens()) {
                String idP = ids.nextToken();
                Part part=this.lookupPart(idP);
                if (part != null)
                    list.add(part);
            }
            product = new Product(id, name, price, inStock, minStock, maxStock, list);
            product.setAssociatedParts(list);
        }
        return product;
    }



    public void writeAll() {

        ClassLoader classLoader = InventoryRepository.class.getClassLoader();
        InputStream in = classLoader.getResourceAsStream(filename);
        File file = new File(String.valueOf(in));
//            File file = new File(classLoader.getResource(filename).getFile()); Modified: replaced by these 2 lines above

        ObservableList<Part> parts = allParts;
        ObservableList<Product> products = allProducts;

//        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))){ Modified
        try (BufferedWriter bw = new BufferedWriter(new FileWriter("src/main/resources/"+filename))){
            for (Part p : parts) {
                String prodStr=p.toString();
                logger.info(prodStr);
                bw.write(p.toString());
                bw.newLine();
            }

            for (Product pr : products) {
                String line = pr.toString() + ",";
                ObservableList<Part> list = pr.getAssociatedParts();
                int index = 0;
                StringBuilder builder = new StringBuilder();
                builder.append(line);
                while (index < list.size() - 1) {
                    builder.append(list.get(index).getID() + ":");
                    index++;
                }
                if (index == list.size() - 1)
                    builder.append(list.get(index).getID());
                bw.write(builder.toString());
                bw.newLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void addPart(Part part) throws Exception {
        String errorMessage = "";
        errorMessage = part.isValid(errorMessage);
        if (errorMessage.length() > 0) {
            throw new Exception(errorMessage);
        }
        allParts.add(part);
        writeAll();
    }

    public void addProduct(Product product) {
        allProducts.add(product);
        writeAll();
    }

    public int getAutoPartId() {
        autoPartId++;
        return autoPartId;
    }

    public int getAutoProductId() {
        autoProductId++;
        return autoProductId;
    }

    public ObservableList<Part> getAllParts() {
        return allParts;
    }

    public ObservableList<Product> getAllProducts() {
        return allProducts;
    }

    public Part lookupPart(String searchItem) {
        for(Part p:allParts) {
            if(p.getName().contains(searchItem) || (p.getID()+"").equals(searchItem)) return p;
        }
        return null;
    }

    public Product lookupProduct(String search) {
        boolean found=false;
        Product product=null;
        for(Product p: allProducts) {
            if(p.getName().contains(search)){
                found=true;
                product=p;
            }
            else if( (p.getID()+"").equals(search)){
                found=true;
                product=p;
            }
        }
        if(found)
            return product;
        else
            return null;
    }

    public void updatePart(int partIndex, Part part) {
        allParts.set(partIndex, part);
        writeAll();
    }

    public void updateProduct(int productIndex, Product product) {
        allProducts.set(productIndex, product);
        writeAll();
    }

    public void deletePart(Part part) {
         allParts.remove(part);
        writeAll();
    }

    public void deleteProduct(Product product) {
        allProducts.remove(product);
        writeAll();
    }
}
