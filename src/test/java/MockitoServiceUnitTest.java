
import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.service.InventoryService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import inventory.repository.InventoryRepository;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

//@ExtendWith(MockitoExtension.class)
@DisplayName("Unit Test Mockito Service")
public class MockitoServiceUnitTest {

    @Mock
    private InventoryRepository inventoryRepository;

    @InjectMocks
    private InventoryService inventoryService;

    @BeforeEach
    void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @AfterEach
    void tearDown(){
    }

    @Test
    @DisplayName("addValidPartService")
    @Order(1)
    void whenAddInhousePart_givenValidInhousePart_returnsVoidAndAddsToList(){
        when(inventoryRepository.getAutoPartId()).thenReturn(100);
        InhousePart part=new InhousePart(100,"part1",20.0,11,3,90,5);
        try {
            Mockito.doNothing().when(inventoryRepository).addPart(part);
        }
        catch (Exception e) {
            Assertions.fail();
        }

        try {
            inventoryService.addInhousePart(part);
        } catch (Exception e) {
            Assertions.fail();
        }
    }


    @Test
    @DisplayName("addInvalidPartService")
    @Order(2)
    void whenAddInhousePart_givenInvalidInhousePart_throwsException(){
        when(inventoryRepository.getAutoPartId()).thenReturn(100);
        InhousePart part=new InhousePart(100,"part1",20.0,11,30,90,5);
        try {
            Mockito.doThrow(new Exception("Inventory level is lower than minimum value. ")).when(inventoryRepository).addPart(part);
        }
        catch (Exception e) {
            Assertions.fail();
        }
        try {
            inventoryService.addInhousePart(part);
            Assertions.fail();
        } catch (Exception e) {
            Assertions.assertEquals(e.getMessage(),"Inventory level is lower than minimum value. ");
        }
    }

}
