
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.when;

@DisplayName("Mockito Repository Unit Tests")
public class MockitoRepositoryUnitTest {

    @InjectMocks
    private InventoryRepository inventoryRepository;

    @Mock
    private Part part;

    @BeforeEach
    void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @AfterEach
    void tearDown(){}

    @Test
    @DisplayName("addValidPart")
    @Order(1)
    void whenAddPart_givenValidPart_returnsVoidAndAddsToList(){
        when(part.isValid("")).thenReturn("");
        int sizeBefore = inventoryRepository.getAllParts().size();
        try {
            inventoryRepository.addPart(part);
            Assertions.assertEquals(sizeBefore + 1, inventoryRepository.getAllParts().size());
        } catch (Exception e) {
            Assertions.fail();
        }
    }

    @Test
    @DisplayName("addInvalidPart")
    @Order(2)
    void whenAddPart_givenInvalidPart_throwsException(){
        when(part.isValid("")).thenReturn("The price must be greater than 0. ");
        int sizeBefore = inventoryRepository.getAllParts().size();
        try {
            inventoryRepository.addPart(part);
            Assertions.fail();
        } catch (Exception e) {
            Assertions.assertEquals(sizeBefore, inventoryRepository.getAllParts().size());
        }
    }
}
