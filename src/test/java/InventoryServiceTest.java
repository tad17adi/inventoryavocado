import inventory.model.InhousePart;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("Service tests")
public class InventoryServiceTest {
    private InventoryRepository inventoryRepository;
    private InventoryService inventoryService;

    @BeforeEach
    void setUp() {
        inventoryRepository = new InventoryRepository();
        inventoryService = new InventoryService(inventoryRepository);
    }

    @AfterEach
    void tearDown() {
    }


    @Test
    @DisplayName("addInhousePartECPFirstTest")
    @Order(1)
    void addInhousePartECPFirstTest() throws Exception {
        int n = inventoryRepository.getAllParts().size();
        InhousePart inhousePart = new InhousePart(0, "TestName01", 2, 30, 1, 400, 5);
        inventoryService.addInhousePart(inhousePart);
        int actual = inventoryRepository.getAllParts().size();
        assertEquals(n + 1, actual);
    }

//    @DisplayName("addInHousePartECPSecondTest")
//    @ParameterizedTest(name = "{index} => name={0}, price={1}, inStock={2}, min={3}, max={4}, partDynamicValue={5}")
//    @CsvSource({
//            "TestName02, 0, 40, 2, 200, 8"
//    })
//    @Order(2)
//    void addInHousePartECPSecondTest(String name, double price, int inStock, int min, int max, int partDynamicValue) {
//        InhousePart inhousePart = new InhousePart(0,name, price, inStock, min, max, partDynamicValue);
//        Exception exception = assertThrows(Exception.class, () -> inventoryService.addInhousePart(inhousePart));
//        assertEquals("The price must be greater than 0. ", exception.getMessage());
//    }

    @Test
    @DisplayName("addInHousePartECPSecondTest")
    @Order(2)
    void addInHousePartECPSecondTest() {
        InhousePart inhousePart = new InhousePart(0, "TestName02", 0, 40, 2, 200, 8);
        Exception exception = assertThrows(Exception.class, () -> inventoryService.addInhousePart(inhousePart));
        assertEquals("The price must be greater than 0. ", exception.getMessage());
    }

    @DisplayName("addInHousePartECPThirdTest")
    @RepeatedTest(value = 3, name = "Repetition {currentRepetition} of {totalRepetitions}")
    @Order(3)
    void addInHousePartECPThirdTest() {
        InhousePart inhousePart = new InhousePart(0, "TestName03", 15, 0, 0, 50, 2);
        Exception exception = assertThrows(Exception.class, () -> inventoryService.addInhousePart(inhousePart));
        assertEquals("Inventory level must be greater than 0. ", exception.getMessage());
    }

    @DisplayName("addInHousePartECPFourthTest")
    @ParameterizedTest(name = "{index} => name={0}, price={1}, inStock={2}, min={3}, max={4}, partDynamicValue={5}")
    @CsvSource({
            "TestName04, -5, -4, 5, 70, 9"
    })
    @Order(4)
    void addInHousePartECPFourthTest(String name, double price, int inStock, int min, int max, int partDynamicValue) {
        InhousePart inhousePart = new InhousePart(0, name, price, inStock, min, max, partDynamicValue);
        Exception exception = assertThrows(Exception.class, () -> inventoryService.addInhousePart(inhousePart));
        assertEquals("The price must be greater than 0. Inventory level must be greater than 0. Inventory level is lower than minimum value. ", exception.getMessage());
    }

    @Test
    @DisplayName("addInhousePartBVAFirstTest")
    @Order(5)
    void addInhousePartBVAFirstTest() throws Exception {
        int n = inventoryRepository.getAllParts().size();
        InhousePart inhousePart = new InhousePart(0, "TestName05", 10, 2, 1, 50, 10);
        inventoryService.addInhousePart(inhousePart);
        int actual = inventoryRepository.getAllParts().size();
        assertEquals(n + 1, actual);
    }

    @Test
    @DisplayName("addInhousePartBVASecondTest")
    @Order(6)
    void addInhousePartBVASecondTest() throws Exception {
        int n = inventoryRepository.getAllParts().size();
        InhousePart inhousePart = new InhousePart(0, "TestName06", 0.02, 30, 2, 70, 4);
        inventoryService.addInhousePart(inhousePart);
        int actual = inventoryRepository.getAllParts().size();
        assertEquals(n + 1, actual);
    }

    @DisplayName("addInHousePartBVAThirdTest")
    @RepeatedTest(value = 3, name = "Repetition {currentRepetition} of {totalRepetitions}")
    @Order(7)
    void addInHousePartBVAThirdTest() {
        InhousePart inhousePart = new InhousePart(0, "TestName07", 20, 0, 0, 30, 2);
        Exception exception = assertThrows(Exception.class, () -> inventoryService.addInhousePart(inhousePart));
        assertEquals("Inventory level must be greater than 0. ", exception.getMessage());
    }

    @DisplayName("addInHousePartBVAFourthTest")
    @ParameterizedTest(name = "{index} => name={0}, price={1}, inStock={2}, min={3}, max={4}, partDynamicValue={5}")
    @CsvSource({
            "TestName08, 0, 40, 0, 100, 3"
    })
    @Order(8)
    void addInHousePartBVAFourthTest(String name, double price, int inStock, int min, int max, int partDynamicValue) {
        InhousePart inhousePart = new InhousePart(0, name, price, inStock, min, max, partDynamicValue);
        Exception exception = assertThrows(Exception.class, () -> inventoryService.addInhousePart(inhousePart));
        assertEquals("The price must be greater than 0. ", exception.getMessage());
    }

}