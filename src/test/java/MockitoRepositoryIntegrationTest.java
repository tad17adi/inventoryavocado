
import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.when;

@DisplayName("Mockito Repository Integration Tests")
public class MockitoRepositoryIntegrationTest {

    @Mock
    private InhousePart part;

    @InjectMocks
    private InventoryRepository inventoryRepository;

    private InventoryService inventoryService;

    @BeforeEach
    void setup(){
        MockitoAnnotations.initMocks(this);
        inventoryService = new InventoryService(inventoryRepository);
    }

    @AfterEach
    void tearDown(){
    }

    @Test
    @DisplayName("addValidPart")
    @Order(1)
    void whenAddInhousePart_givenValidInhousePart_returnsVoidAndAddsToList(){

        when(part.isValid("")).thenReturn("");
        int sizeBefore = inventoryRepository.getAllParts().size();
        try {
            inventoryService.addInhousePart(part);
            Assertions.assertEquals(sizeBefore + 1, inventoryRepository.getAllParts().size());
        } catch (Exception e) {
            Assertions.fail();
        }
    }


    @Test
    @DisplayName("addInvalidPart")
    @Order(2)
    void whenAddInhousePart_givenInvalidInhousePart_throwsException(){

        when(part.isValid("")).thenReturn("The price must be greater than 0. ");
        int sizeBefore = inventoryRepository.getAllParts().size();
        try {
            inventoryService.addInhousePart(part);
            Assertions.fail();
        } catch (Exception e) {
            Assertions.assertEquals(sizeBefore, inventoryRepository.getAllParts().size());
        }
    }

}
