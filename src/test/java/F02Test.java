import inventory.model.Product;
import inventory.repository.InventoryRepository;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;


@DisplayName("F02 tests")
public class F02Test {
    private InventoryRepository inventoryRepository;

    @BeforeEach
    void setUp() {
        inventoryRepository = new InventoryRepository();
    }

    @AfterEach
    void tearDown() {
    }


    @Test
    @DisplayName("lookupProductNameFound")
    @Order(1)
    void lookupProductNameFoundTest() {
        Product product = inventoryRepository.lookupProduct("First");
        Assertions.assertNotEquals(null, product);
    }

    @Test
    @DisplayName("lookupProductEmptyString")
    @Order(2)
    void lookupProductEmptyStringTest() {
        Product product = inventoryRepository.lookupProduct("");
        Assertions.assertNotEquals(null, product);
    }

    @Test
    @DisplayName("lookupProductIdFound")
    @Order(3)
    void lookupProductIdFoundTest() {
        Product product = inventoryRepository.lookupProduct("41");
        Assertions.assertNotEquals(null, product);
    }

    @Test
    @DisplayName("lookupProductNotFound")
    @Order(4)
    void lookupProductNotFoundTest() {
        Product product = inventoryRepository.lookupProduct("abcd");
        assertEquals(null, product);
    }
}
