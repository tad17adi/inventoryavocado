
import inventory.model.InhousePart;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.when;

@DisplayName("Mockito Part Integration Tests")
public class MockitoPartIntegrationTest {

    private InhousePart part;

    private InventoryRepository inventoryRepository;

    private InventoryService inventoryService;

    @BeforeEach
    void setup(){
        inventoryRepository = new InventoryRepository();
        inventoryService = new InventoryService(inventoryRepository);
        part = new InhousePart(inventoryRepository.getAutoPartId(),"PartIntegrationTest",50.0,30,5,60,2);
    }

    @Test
    @DisplayName("addValidPart")
    @Order(1)
    void whenAddInhousePart_givenValidInhousePart_returnsVoidAndAddsToList(){

        int sizeBefore = inventoryRepository.getAllParts().size();
        try {
            inventoryService.addInhousePart(part);
            Assertions.assertEquals(sizeBefore + 1, inventoryRepository.getAllParts().size());
        } catch (Exception e) {
            Assertions.fail();
        }
    }


    @Test
    @DisplayName("addInvalidPart")
    @Order(2)
    void whenAddInhousePart_givenInvalidInhousePart_throwsException(){

        int sizeBefore = inventoryRepository.getAllParts().size();
        try {
            part.setInStock(2);
            inventoryService.addInhousePart(part);
            Assertions.fail();
        } catch (Exception e) {
            Assertions.assertEquals(sizeBefore, inventoryRepository.getAllParts().size());
        }
    }

}
